import { PayloadCreateUser, PayloadEditUser, PayloadLoginUser, PayloadResetPasswordUser } from '@/types/types';
import axios from 'axios';

// const baseURL = 'http://localhost:3000';
const baseURL = 'https://full-stack-test-back-deploy.onrender.com';

export const createUser = async (user: PayloadCreateUser) => {
    try {
        const {name, email, password} = user
        const response = await axios.post(`${baseURL}/api/users`, {
            name,
            email,
            password
        });

        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not create user");
    }
}

export const editUser = async (user: PayloadEditUser, token: string, id: number) => {
    try {
        const payload: Partial<PayloadEditUser> = {};
        if (user.name) payload.name = user.name;
        if (user.email) payload.email = user.email;
        if (user.password) payload.password = user.password;

        const response = await axios.put(
            `${baseURL}/api/users/${id}`,
            payload,
            {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            }
        );

        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not edit user");
    }
}

export const loadUserById = async (token: string, id: number) => {
    try {
        const response = await axios.get(`${baseURL}/api/users/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not load user");
    }
}

export const removeUser = async (token: string, id: number) => {
    try {
        const response = await axios.delete(`${baseURL}/api/users/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not remove user");
    }
}

export const auth = async (user: PayloadLoginUser) => {
    try {
        const response = await axios.post(`${baseURL}/api/auth`, {
            email: user.email,
            password: user.password,
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not login user");
    }
}

export const loadMovies = async (token: string) => {
    try {
        const response = await axios.get(`${baseURL}/api/movies`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not load movies");
    }
}

export const loadMovieById = async (token: string, id: string) => {
    try {
        const response = await axios.get(`${baseURL}/api/movies/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not load movie");
    }
}

export const loadUserByEmail = async (email: string) => {
    try {
        const response = await axios.get(`${baseURL}/api/users/mail/${email}`);
        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not load user");
    }
}

export const resetPassword = async (user: PayloadResetPasswordUser) => {
    try {
        const { password } = user
        const response = await axios.put(`${baseURL}/api/users/reset-password/${user.email}`, {
            password
        });

        return response.data;
    } catch (error) {
        if (axios.isAxiosError(error)) {
            return error.response;
        }
        throw new Error("Could not edit user");
    }
}