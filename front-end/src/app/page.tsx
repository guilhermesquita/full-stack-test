'use client'
import CardMovie from "@/components/cardMovie";
import Header from "@/components/header";
import { loadMovies, loadUserById } from "@/service/api";
import { useEffect, useState } from "react";
import * as jwt from 'jsonwebtoken'
import Spinner from "@/components/spinner";

type IMovie = {
  id_movie: string;
  title_movie: string;
  director_movie: string;
  description_movie: string;
  image_movie: string;
  year_movie: string;
}

type IUser = {
  id_user: number,
  nm_user: string,
  email_user: string,
  password_user: string,
  created_at: string
}

type jwtObject = {
  exp: number
  iat: number
  key: number
}

export default function Home() {

  const [movies, setMovies] = useState<IMovie[] | []>([])
  const [user, setUser] = useState<IUser>()

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const token = localStorage.getItem("token");
      if (!token) {
        window.location.href = '/login';
        return;
      }

      let decodeToken: jwtObject | null = null;
      try {
        decodeToken = jwt.decode(token as string) as jwtObject;
        if (!decodeToken || decodeToken.exp * 1000 < Date.now()) {
          window.location.href = '/login';
          return;
        }
      } catch (error) {
        window.location.href = '/login';
        return;
      }

      const id = decodeToken.key;

      (async () => {
        const data = await loadMovies(token);
        const userById = await loadUserById(token, id);
        setUser(userById);
        setMovies(data);
      })();
    }
  }, []);

  const callbackLoad = () => {
    return <Spinner />
  }

  return (
    <main className="w-full flex flex-col">
      <Header key={user?.id_user} name={user?.nm_user} id={user?.id_user} />
      <section className="w-full flex flex-row flex-wrap justify-center items-center gap-1 pt-10">
        {movies.length > 0 ? movies.map((movie: IMovie) => {
          return (
            <CardMovie key={movie.id_movie}
              nameMovie={movie.title_movie}
              idMovie={movie.id_movie}
              descriptionMovie={movie.description_movie}
              yearMovie={movie.year_movie} directorMovie={movie.director_movie} imageMovie={movie.image_movie}
            />
          )
        }) : callbackLoad()}
      </section>
    </main>
  );
}