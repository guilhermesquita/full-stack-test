'use client'
import { loadUserById, removeUser } from "@/service/api"
import '@/app/globals.css'
import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import DataUser from "@/components/dataUser"
import Header from "@/components/header"
import AngleLeft from "@/components/angleLeft"
import { useModal } from "@/service/modalService"
import { Modal } from "@/components/Modal"
import Spinner from "@/components/spinner"

type IUser = {
    id_user: number,
    nm_user: string,
    email_user: string,
    password_user: string,
    created_at: string
}

export default function UserById () {
    const router = useRouter()
    const { id } = router.query;

    const [users, setUsers] = useState<IUser | null>(null)
    const [isModalOpen, openModal, closeModal] = useModal();
    const [errorMessage, setErrorMessage] = useState<string>('')

    useEffect(() => {
        const fetchData = async () => {
            if (id && typeof id === 'string') {
                const token = localStorage.getItem("token") as string;
                const user = await loadUserById(token, Number(id));
                setUsers(user);
            }
        }

        fetchData();
    }, [id]);

    const onSubmitRemoveUser = async () => {
        const token = localStorage.getItem("token") as string;
        const savedUser = await removeUser(token, Number(id))

        if (savedUser.status === 401) {
            return setErrorMessage('Você não tem autorização para remover o usuário!')
        }

        if (savedUser.status === 406) {
            return setErrorMessage('Usuário não encontrado!')
        }

        localStorage.setItem('token', '');
        router.push('/login');
        return savedUser
    };

    const logoutUser = async () => {
        localStorage.setItem('token', '');
        router.push('/login');
    }

    const calbackLoad = () => {
        return <Spinner />
      }

    return (
        <div>
            <Header />
            <AngleLeft content="meus dados" />
            <article className="w-full flex flex-col justify-center items-center">
                {users ? (
                    <>
                        <section className="w-full flex justify-center">

                            <DataUser key={users.id_user} name={users.nm_user}
                                email={users.email_user} password={users.password_user} />
                        </section>

                        <section className="flex w-full flex-row items-center justify-center gap-3">
                            <span className="hidden sm:block">
                                <button type="button" onClick={openModal} className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset duration-500 ring-gray-300 hover:bg-gray-50">
                                    <svg className="-ml-0.5 mr-1.5 h-5 w-5 text-gray-400" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path d="M2.695 14.763l-1.262 3.154a.5.5 0 00.65.65l3.155-1.262a4 4 0 001.343-.885L17.5 5.5a2.121 2.121 0 00-3-3L3.58 13.42a4 4 0 00-.885 1.343z" />
                                    </svg>

                                    Editar usuário
                                </button>
                            </span>
                            <span className="hidden sm:block">
                                <button type="button" onClick={logoutUser} className="inline-flex items-center rounded-md bg-white px-3 py-2 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset duration-500 ring-gray-300 hover:bg-gray-50">
                                    <svg className="-ml-0.5 mr-1.5 h-5 w-5 text-gray-400" fill="currentColor" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 122.88 108.06"><title>back-arrow</title><path d="M63.94,24.28a14.28,14.28,0,0,0-20.36-20L4.1,44.42a14.27,14.27,0,0,0,0,20l38.69,39.35a14.27,14.27,0,0,0,20.35-20L48.06,68.41l60.66-.29a14.27,14.27,0,1,0-.23-28.54l-59.85.28,15.3-15.58Z" /></svg>

                                    Sair da conta
                                </button>
                            </span>
                            <span className="hidden sm:block">
                                <button type="button" onClick={onSubmitRemoveUser} className="inline-flex items-center rounded-md bg-[#b84747] px-3 py-2 text-sm font-semibold text-white shadow-sm ring-1 ring-inset duration-500 ring-gray-300 hover:bg-gray-50 hover:text-black">
                                    <svg className="-ml-0.5 mr-1.5 h-5 w-5 text-white" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true"
                                        xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="100" height="100">
                                        <path d="M 13 3 A 1.0001 1.0001 0 0 0 11.986328 4 L 6 4 A 1.0001 1.0001 0 1 0 6 6 L 24 6 A 1.0001 1.0001 0 1 0 24 4 L 18.013672 4 A 1.0001 1.0001 0 0 0 17 3 L 13 3 z M 6 8 L 6 24 C 6 25.105 6.895 26 8 26 L 22 26 C 23.105 26 24 25.105 24 24 L 24 8 L 6 8 z"></path>
                                    </svg>

                                    Remover Usuário
                                </button>
                            </span>
                        </section>
                        {errorMessage.length > 1 && <p className="text-red-500">{errorMessage}</p>}
                    </>
                ): calbackLoad()}
                <Modal isOpen={isModalOpen} user={users}
                    closeModal={closeModal} contentTitle="Editar Usuário" />
            </article>
        </div>
    )
}