import '@/app/globals.css'
import Header from "@/components/header";
import RegisterForm from '@/components/registerForm';
import { AuthProvider } from '@/context/AuthContext';

export default function RegisterPage() {
    return (
        <main className='h-full'>
            <AuthProvider>
                <Header />
                <RegisterForm />
            </AuthProvider>
        </main>
    )
}