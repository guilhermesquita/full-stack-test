'use client'
import Header from "@/components/header";
import '@/app/globals.css'
import { loadMovieById } from "@/service/api";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import AngleLeft from "@/components/angleLeft";
import DataMovie from "@/components/dataMovie";
import Spinner from "@/components/spinner";


type IMovie = {
    id_movie: string;
    title_movie: string;
    director_movie: string;
    description_movie: string;
    image_movie: string;
    year_movie: string;
}

export default function MovieById() {

    const router = useRouter()
    const { id } = router.query;

    const [movies, setMovies] = useState<IMovie | null>(null)

    useEffect(() => {
        const fetchData = async () => {
            if (id && typeof id === 'string') {
                const token = localStorage.getItem("token") as string;
                const movie = await loadMovieById(token, id);
                setMovies(movie);
            }
        }

        fetchData();
    }, [id]);

    const calbackLoad = () => {
        return <Spinner/>
    }

    return (
        <div>
            <Header />
            <AngleLeft content="filmes"/>
            <section className="w-full flex items-center justify-center">
                {movies ? (
                    <div className="w-full flex justify-center">
                        <DataMovie key={movies.id_movie}
                        idMovie={movies.id_movie} descriptionMovie={movies.description_movie}
                        nameMovie={movies.title_movie} yearMovie={movies.year_movie} 
                        directorMovie={movies.director_movie} imageMovie={movies.image_movie}
                        />
                    </div>
                ) : calbackLoad()}
            </section>
        </div>
    )
} 