import '@/app/globals.css'
import CheckEmailForm from '@/components/checkEmailForm'
import Header from '@/components/header'
import { AuthProvider } from '@/context/AuthContext'

export default function LoginPage() {
    return (
        <main className='h-full'>
            <AuthProvider>
                <Header />
                <CheckEmailForm />
            </AuthProvider>
        </main>
    )
}