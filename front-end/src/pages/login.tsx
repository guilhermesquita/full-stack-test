import '@/app/globals.css'
import Header from '@/components/header'
import LoginForm from '@/components/loginForm'
import { AuthProvider } from '@/context/AuthContext'

export default function LoginPage() {
    return (
        <main className='h-full'>
            <AuthProvider>
                <Header />
                <LoginForm />
            </AuthProvider>
        </main>
    )
}