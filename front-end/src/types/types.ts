export interface PayloadCreateUser {
    name: string;
    email: string;
    password: string;
}

export interface PayloadEditUser {
    id: number;
    name: string;
    email: string;
    password: string;
}

export interface PayloadResetPasswordUser {
    email: string;
    password: string;
}

export interface PayloadLoginUser {
    email: string;
    password: string;
}