'use client'

import '@/app/globals.css'
import { AuthContext } from '@/context/AuthContext'
import { auth } from '@/service/api'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import { useContext } from 'react'
import { useForm } from 'react-hook-form'
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'; 
import * as Yup from 'yup'
import Spinner from './spinner'
import Link from 'next/link'

export default function LoginForm() {
    const context = useContext(AuthContext)

    const router = useRouter()

    const schema = Yup.object().shape({
        email: Yup.string().required('Email é obrigatório').email('Email inválido'),
        password: Yup.string().min(4, 'A senha dever ter mais de 3 caracteres').required('Senha é obrigatório'),
    })

    const { register, handleSubmit, formState, reset } = useForm({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues: {
            email: '',
            password: ''
        }
    })
    const { errors, isSubmitting, isDirty, isValid } = formState

    const disableStyle = "flex w-1/3 cursor-not-allowed justify-center rounded-md bg-[#eee] px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
    const enableStyle = "flex w-1/3 justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"

    const onSubmit = async (data: any) => {
        const savedUser = await auth(data)
        if (savedUser.status === 406) {
            return toast.error('Email ou senha incorreta')
        }
        context?.handleGetToken(savedUser.token)
        reset()
        router.push('/');
        return savedUser
    };

    const calbackLoad = () => {
        return <Spinner />
      }

    return (
        <form className='w-full flex flex-col h-full
        justify-center items-center gap-10 pt-10'>
            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Email</label>
                <div className="mt-2.5">
                    <input type="email" {...register("email", { required: true })}
                        placeholder='seuemail@mail.com'
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.email && <p>{errors.email?.message}</p>}
                </div>
            </div>

            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Senha</label>
                <div className="mt-2.5">
                    <input type="password" {...register("password", { required: true })}
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.password && <p>{errors.password?.message}</p>}
                </div>
            </div>


            <button type='button'
                className={!isDirty || !isValid ? disableStyle : enableStyle}
                disabled={!isDirty || !isValid}
                onClick={() => handleSubmit(onSubmit)()}
            >{isSubmitting ? calbackLoad() : 'Entrar'}</button>
            <ToastContainer/>
            <article className='flex gap-5'>
                <Link href="/password-reset" className="font-semibold text-indigo-600 hover:text-indigo-500">Esqueceu a senha?</Link>
                <Link href="/register" className="font-semibold text-[#333333] hover:text-[#000000] duration-500">Cadastrar-se</Link>
            </article>
        </form>
    )
}