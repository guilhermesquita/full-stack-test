import '@/app/globals.css'
import * as Yup from 'yup'
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { createUser } from '@/service/api';
import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react';
import { AuthContext } from '@/context/AuthContext';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'; 
import Spinner from './spinner';
import Link from 'next/link';

export default function RegisterForm() {

    const context = useContext(AuthContext)

    const router = useRouter()

    const schema = Yup.object().shape({
        name: Yup.string().required('Name is required'),
        email: Yup.string().required('Email é obrigatório').email('Email inválido'),
        password: Yup.string().min(3, 'A senha deve ter no mínimo 3 caracteres').required('Password is required'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password')], 'A senhas devem ser iguais')
            .required('Campo obriatório'),
    })

    const { register, handleSubmit, formState, reset } = useForm({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        }
    })

    const disableStyle = "flex w-1/3 cursor-not-allowed justify-center rounded-md bg-[#eee] px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
    const enableStyle = "flex w-1/3 justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"

    const { errors, isSubmitting, isDirty, isValid } = formState

    const onSubmit = async (data: any) => {
        const savedUser = await createUser(data)
        if (savedUser.status === 409) {
            return toast.error('Email já cadastrado')
        }
        context?.handleGetToken(savedUser.token)
        reset()
        router.push('/');
        return savedUser
    };

    const calbackLoad = () => {
        return <Spinner />
      }

    return (
        <form className='w-full flex flex-col h-full
        justify-center items-center gap-5 pt-10'>

            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Email</label>
                <div className="mt-2.5">
                    <input type="email" {...register("email", { required: true })}
                        placeholder='seuemail@mail.com'
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.email && <p>{errors.email?.message}</p>}
                </div>
            </div>

            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Nome</label>
                <div className="mt-2.5">
                    <input type="text" {...register("name", { required: true })}
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.name && <p>{errors.name?.message}</p>}
                </div>
            </div>

            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Senha</label>
                <div className="mt-2.5">
                    <input type="password" {...register("password", { required: true })}
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.password && <p>{errors.password?.message}</p>}
                </div>
            </div>

            <div className="flex flex-col w-1/3">
                <label className="block text-sm font-semibold leading-6 text-gray-900">Confirmar Senha</label>
                <div className="mt-2.5">
                    <input type="password" {...register("confirmPassword", { required: true })}
                        className="block w-full rounded-md border-0 px-3.5 py-2 
                    text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 
                    placeholder:text-gray-400 focus:ring-2 focus:ring-inset 
                    focus:ring-indigo-600 duration-500 sm:text-sm sm:leading-6"/>
                    {errors.confirmPassword && <p>{errors.confirmPassword?.message}</p>}
                </div>
            </div>

            <button type='button'
                className={!isDirty || !isValid ? disableStyle : enableStyle}
                disabled={!isDirty || !isValid}
                onClick={() => handleSubmit(onSubmit)()}
            >{isSubmitting ? calbackLoad() : 'Enviar'}</button>
            <ToastContainer/>
            <Link href='/login'
            className="flex w-1/3 justify-center rounded-md duration-500 bg-[#000] px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >entrar em uma conta</Link>
        </form>
    )
}