import { editUser } from "@/service/api";
import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css'; 
import * as Yup from 'yup'

type IUser = {
    id_user: number,
    nm_user: string,
    email_user: string,
    password_user: string,
    created_at: string
}

type ModalProps = {
    isOpen: boolean;
    closeModal: () => void;
    contentTitle: string;
    user: IUser | null;
};

export const Modal = ({ isOpen, closeModal, contentTitle, user }: ModalProps) => {

    const [errorMessageEmail, setErrorMessageEmail] = useState(''); 

    const schema = Yup.object().shape({
        name: Yup.string().notRequired(),
        email: Yup.string().email('Email inválido').notRequired(),
        password: Yup.string().notRequired(),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password')], 'Passwords must match')
            .notRequired(),
    })

    const { register, handleSubmit, formState, reset } = useForm({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues: {
            name: '',
            email: '',
            password: '',
            confirmPassword: ''
        }
    })

    const { errors, isSubmitting, isDirty, isValid } = formState

    // const [serviceId, incrementId] = useIdService();

    const onSubmit = async (data: any) => {
        const token = localStorage.getItem("token") as string;

        const editedUser = await editUser(data, token, Number(user?.id_user))

        if (editedUser.status === 401){
            return toast.error('Não autorizado. Tente mais tarde')
        }
        if (editedUser.status === 409){
            return toast.error('Email já cadastrado')
        }
        closeModal()
        window.location.reload();
    };



    if (isOpen) {
        return (
            <div className="fixed top-0 bottom-0 left-0 right-0 bg-bgModal z-[10">
                <div className="fixed top-2/4 left-2/4" style={{ transform: 'translate(-50%, -50%)' }}>
                    <section className="bg-[#FFFFFF] flex flex-col w-[32rem] p-5 gap-5 border-4 border-black">
                        <h3 className="font-IBM font-bold text-4xl">{contentTitle}</h3>
                        <form
                            className="flex flex-col w-full justify-center items-center gap-5">

                            <input type="email"
                                {...register("email", { required: true })} placeholder={user?.email_user}
                                className="w-full h-12 placeholder:font-IBM placeholder:font-bold rounded-2xl outline-0 border-2 items-center gap-2 pl-5 duration-500 ${
                                border-black h-16"/>

                            <input type="name"
                                {...register("name", { required: true })} placeholder={user?.nm_user}
                                className="w-full h-12 placeholder:font-IBM placeholder:font-bold rounded-2xl outline-0 border-2 items-center gap-2 pl-5 duration-500 ${
                                border-black h-16"/>

                            <article className="w-full flex justify-between items-center gap-10">
                                <input type="password" placeholder="senha"
                                    className="outline-0 w-3/5 border-b-2 border-black"
                                    {...register("password")}
                                />
                                {errors.password && <p>{errors.password?.message}</p>}
                                <input type="password" placeholder="Confirmar Senha"
                                    className="outline-0 w-3/5 border-b-2 border-black"
                                    {...register("confirmPassword")}
                                />
                                {errors.confirmPassword && <p>{errors.confirmPassword?.message}</p>}
                            </article>
                            <article className="flex gap-5">
                                <button type="button"
                                className="font-IBM font-bold text-xl hover:text-[#484040] text-[#141212]"
                                    onClick={() => handleSubmit(onSubmit)()}
                                >avançar</button>
                                <button type="button"
                                    onClick={closeModal} className="font-IBM font-bold text-xl text-[#656161]">cancelar</button>
                            </article>
                        </form>
                        <ToastContainer/>
                    </section>
                </div>
            </div>
        );
    }
    return null;
};