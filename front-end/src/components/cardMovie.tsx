export interface IProps {
    idMovie: string
    nameMovie: string,
    directorMovie: string,
    descriptionMovie: string,
    imageMovie: string,
    yearMovie: string,
}

export default function CardMovie(props: IProps) {
    return (
        <article className="flex w-full gap-2 justify-center items-center pb-10">
            <div className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                <a href={`movies/${props.idMovie}`}>
                    <img className="rounded-t-lg" src={props.imageMovie} alt="" />
                </a>
                <div className="p-5">
                    <a href={`movies/${props.idMovie}`}>
                        <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{props.nameMovie} ({props.yearMovie})</h5>
                    </a>
                    <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">diretor: {props.directorMovie}</p>
                    <a href={`movies/${props.idMovie}`} className="inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white duration-500 bg-indigo-600 rounded-lg hover:bg-black focus:outline-none">
                        Ler mais
                        <svg className="rtl:rotate-180 w-3.5 h-3.5 ms-2" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 10">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5h12m0 0L9 1m4 4L9 9" />
                        </svg>
                    </a>
                </div>
            </div>
        </article>
    )
}