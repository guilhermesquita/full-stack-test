import { IProps } from "./cardMovie";

export default function DataMovie(props: IProps) {
    return (
        <article className="block max-w-4xl p-6 bg-white border border-gray-200 shadow ">
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-black">{props.nameMovie}</h5>
            <p className="font-normal text-gray-700 dark:text-gray-400"><strong>Diretor(a): {props.directorMovie}</strong></p>
            <p className="font-normal text-gray-700">{props.descriptionMovie}</p>
        </article>
    )
}