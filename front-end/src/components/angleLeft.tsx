import '@/app/globals.css'
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

interface IProps{
    content: string;
}

export default function AngleLeft(props: IProps) {
    return (
        <article className="flex gap-2 w-full p-10 items-center">
                <Link href='/' className="w-16 h-12 rounded-lg border-2 border-[#BEBEBE] flex
                justify-center items-center">
                    <FontAwesomeIcon icon={faAngleLeft} style={{ color: "#000000", height: '30px' }} />
                </Link>
                <h1 className="text-3xl font-IBM font-bold">Visualizar {props.content}</h1>
            </article>
    )
}