interface IProps {
    id?: number
    name?: string
}

export default function Header(props: IProps) {

    const showName = () => {
        if (props.name?.length !== undefined) {
            return (
                <article className="flex flex-col">
                    <a> Olá, {props.name} :)</a>
                    <a className="text-sm text-[#4b4a4a]" href={`users/${props.id}`}>ver perfil</a>
                </article>
            )
        }
    }

    return (
        <header className="bg-black text-[#FFFFFF] h-20 flex items-center pl-5 font-bold pr-20">
            <article className="flex justify-between w-full h-full items-center">
                <h1>STREAMER - Aplicação Teste WatchBR</h1>
                {showName()}
            </article>
        </header>
    )
}