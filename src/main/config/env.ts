export const env = {
  port: process.env.PORT ?? 3000,
  jwtSecret: process.env.JWT_SECRET,
  postgres_host: process.env.POSTGRES_HOST ?? 'dpg-cpmcsm5ds78s73ag65sg-a.ohio-postgres.render.com',
  postgres_port: process.env.POSTGRES_PORT ?? 5432,
  postgres_username: process.env.POSTGRES_USERNAME ?? 'watch_deploy_pg_user',
  postgres_password: process.env.POSTGRES_PASSWORD ?? 'as4MmAfnqVKtH1FrnmnEDF5qkj895Pkh',
  postgres_database: process.env.POSTGRES_DATABASE ?? 'watch_deploy_pg',
  postgres_synchronize: handleBoolean(process.env.POSTGRES_SYNCHRONIZE),
  postgres_logging: handleBoolean(process.env.POSTGRES_LOGGING),
}

function handleBoolean(
  env_variable: string | undefined,
  default_value: boolean = true
): boolean {
  if (!env_variable) {
    return default_value
  } else {
    return env_variable === 'true'
  }
}
